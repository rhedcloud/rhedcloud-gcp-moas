@echo off

echo OpenEAI Example Enterprise Build System
echo ---------------------------------------
echo.

set OPENEAI_HOME=./

if "%JAVA_HOME%" == "" goto error
if "%OPENEAI_HOME%" == "" goto openeai_error

set OPENEAI_RUNTIME=%OPENEAI_HOME%\configs\messaging\Environments\Examples\
REM set OPENEAI_LIB=%OPENEAI_HOME%\lib
set BUILD_FILE=%OPENEAI_HOME%\build.xml
REM set LOCALCLASSPATH=%JAVA_HOME%\lib\tools.jar;%OPENEAI_LIB%\ant.jar;
set LOCALCLASSPATH=%JAVA_HOME%\lib\tools.jar;%OPENEAI_HOME%\lib\ant.jar
REM set ANT_HOME=%OPENEAI_LIB%
set ANT_HOME=%OPENEAI_HOME%\lib

echo Building with classpath %LOCALCLASSPATH%
echo.

echo Starting Ant...

"%JAVA_HOME%\bin\java.exe" -Dant.home="%ANT_HOME%" -classpath "%LOCALCLASSPATH%" org.apache.tools.ant.Main -buildfile %BUILD_FILE% %1 %2 %3 %4 %5

goto end

:error

echo ERROR: JAVA_HOME not found in your environment.
echo Please, set the JAVA_HOME variable in your environment to match the
echo location of the Java Virtual Machine you want to use.

:openeai_error

echo ERROR: OPENEAI_HOME not found in your environment.
echo Please, set the OPENEAI_HOME variable in your environment to match the
echo location of the OpenEAI Examples installation directory you want to use.

:end